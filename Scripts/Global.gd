extends Node


# енум имен сцен
enum VIEW_MAP {
	START,
	GAME,
	INFO,
	LEVELS,
	LEVEL_MENU,
}
# имена всех сцен
const VIEWS: PoolStringArray = PoolStringArray([
	"res://Scenes/Start.tscn",
	"res://Scenes/Game.tscn",
	"res://Scenes/Info.tscn",
	"res://Scenes/Levels.tscn",
	"res://Scenes/LevelMenu.tscn",
])

# GUI
const GIFT_PRICE: int = 60000 # gift price


# GAME
const SPIN_TIME: float = 2.3 # время кручения после разгона
# const STOPPING_SPIN_TIME: float = 1.5 # время остановки кручения
var AUTO_SPIN_COUNT: int = 1 # счетчик количества повторов автоспина
const AUTO_SPIN_MAX: float = 10.0 # количество повторов автоспина

const BGS: Array = [
	preload("res://Resources/Sprites/bg_lvl_1-min.png"),
	#preload("res://Resources/Sprites/bg_lvl_2-min.png"),
	#preload("res://Resources/Sprites/bg_lvl_3-min.png"),
	#preload("res://Resources/Sprites/bg_lvl_4-min.png"),
	#preload("res://Resources/Sprites/bg_lvl_5-min.png"),
]
const BOARDS: Array = [
	preload("res://Resources/Sprites/board_lvl_1-min.png"),
	#preload("res://Resources/Sprites/board_lvl_2-min.png"),
	#preload("res://Resources/Sprites/board_lvl_3-min.png"),
	#preload("res://Resources/Sprites/board_lvl_4-min.png"),
	#preload("res://Resources/Sprites/board_lvl_5-min.png"),
]
const SHADOWS: Array = [
	[
#		preload("res://source/assets/image/shadow_top_lvl_1.png"),
#		preload("res://source/assets/image/shadow_bottom_lvl_1.png"),
		null, null,
	],
	#[
	#	null, null,
	#],
	#[
	#	null, null,
	#],
]

const PROPS: = [
	{
		w = 110,
		h = 110,
		count_x = 3, # количество по горизонтали
		count_y = 3, # количество по вертикали
		offset_x = 8, # отступ между иконками
		offset_y = 8,
		variations = 9, # вариации иконок
		icons_offset_x = 0, # отступ иконок
		icons_offset_y = -6,
		board_bg_offset_x = 0, # отступ картинки доски
		board_bg_offset_y = 0,
		shadow_top_offset_y = 0, # отступ верхней тени
		shadow_bottom_offset_y = 0, # отступ нижней тени
	},
	#	{
	#		w = 207,
	#		h = 170,
	#		count_x = 3,
	#		count_y = 3,
	#		offset_x = 15,
	#		offset_y = 4,
	#		variations = 3,
	#		icons_offset_x = 0,
	#		icons_offset_y = -60,
	#		board_bg_offset_x = 0,
	#		board_bg_offset_y = -60,
	#		shadow_top_offset_y = 0,
	#		shadow_bottom_offset_y = 0,
	#	},
]


# METHODS -------------------------


func set_autospin_count(count: int) -> void:
	AUTO_SPIN_COUNT = count


