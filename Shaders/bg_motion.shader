shader_type canvas_item;

uniform vec2 tiled_factor = vec2(2.0, 1.5);
uniform vec2 offset_scale = vec2(5.0, 5.0);

void fragment() {
	vec2 tiled_uvs = UV * tiled_factor;
	tiled_uvs.y *= 0.6;
	vec2 uv_offset;
	uv_offset.x = cos(TIME + (tiled_uvs.x + tiled_uvs.y) * offset_scale.x);
	uv_offset.y = sin(TIME + (tiled_uvs.x + tiled_uvs.y) * offset_scale.y);
	COLOR = texture(TEXTURE, UV + uv_offset * 0.015);
}