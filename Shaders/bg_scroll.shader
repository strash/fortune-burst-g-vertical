shader_type canvas_item;

const float SPEED = 0.4;

vec4 set_color(inout vec4 c, in float t) {
	c.r = sin(t * SPEED);
	c.b = cos(t * SPEED);
	return clamp(c, 0.0, 1.0);
}

void fragment() {
	vec2 offset = UV;
	offset.x -= TIME * SPEED;
	offset.y += TIME * SPEED;
	vec2 uv = vec2(offset);

	vec4 color = texture(TEXTURE, uv);
	COLOR = set_color(color, TIME);
}
